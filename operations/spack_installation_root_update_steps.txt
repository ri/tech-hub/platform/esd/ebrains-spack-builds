1. Change in CI/CD variables
INSTALLATION_ROOT="main-spack-instance-2205"

2. Start an OpenShift build job from a terminal

oc create -f simplejob.yml

3. Execute in the running pod:

mkdir -p /opt/app-root/src
cd /srv
mkdir -p $INSTALLATION_ROOT
cd $INSTALLATION_ROOT
git clone https://github.com/spack/spack.git
git checkout -b release_v0_2_spack_commit a8d440d3ababcdec20d665ad938ab880cd9b9d17
cat <<EOF > /srv/$INSTALLATION_ROOT/spack/etc/spack/packages.yaml
packages:
  all:
    target: [x86_64]
EOF
source /srv/$INSTALLATION_ROOT/spack/share/spack/setup-env.sh
cat <<EOF > $SPACK_ROOT/etc/spack/defaults/mirrors.yaml
mirrors:
  public_mirror: https://spack-llnl-mirror.s3-us-west-2.amazonaws.com/
  spack-public: https://mirror.spack.io
EOF
spack compiler find
spack install gcc@10.3.0
spack load gcc@10.3.0
spack compiler find
spack install python@3.8.11 %gcc@10.3.0
cp -r ~/.spack $SPACK_ROOT
mkdir -p /srv/jupyterlab_kernels/prod/experimental