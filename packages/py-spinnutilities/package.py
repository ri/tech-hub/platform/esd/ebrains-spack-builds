# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PySpinnutilities(PythonPackage):
    """This provides basic utility functions and classes to other parts of
    SpiNNaker"s tooling. Nothing in here knows anything about SpiNNaker
    functionality."""

    homepage = "https://github.com/SpiNNakerManchester/SpiNNUtils"
    pypi = "SpiNNUtilities/SpiNNUtilities-1!7.0.0.tar.gz"

    def url_for_version(self, version):
        name = "spinnutilities" if version >= Version("7.2.0") else "SpiNNUtilities"
        url = "https://pypi.org/packages/source/s/SpiNNUtilities/{}-1!{}.tar.gz"
        return url.format(name, version)

    version("7.3.0", sha256="5343004fd2aeec0124267e91c2649356b20bf8f2a5d33c9d7cd5ea6cce7dd86b")
    version("7.0.0", sha256="662855395ec367008735047a66a7ca75d1e5070e309ca3aa6ba3a843fb722841")

    depends_on("python@3.8:", type=("build", "run"), when="@7.3.0:")
    depends_on("python@3.7:", type=("build", "run"), when="@7.0.0:")

    depends_on("py-appdirs", type=("build", "run"))
    depends_on("py-numpy", type=("build", "run"))
    depends_on("py-pyyaml", type=("build", "run"))
    depends_on("py-requests", type=("build", "run"))
