# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyErrr(PythonPackage):
    """Elegant Python exception factories"""

    homepage = "https://github.com/Helveg/errr"
    pypi = "errr/errr-1.2.0.tar.gz"

    maintainers = ["helveg"]

    version("1.2.0", sha256="2824b83edbd1f61d354c60e36400b2e31ab83e2094acb837b86686892b866e76")

    depends_on("py-setuptools", type="build")
