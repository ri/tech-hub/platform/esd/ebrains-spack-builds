# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyEbrainsKgCore(PythonPackage):
    """A Python wrapper for the KG core API incl. token handling."""

    homepage = "https://github.com/HumanBrainProject/kg-core-sdks"
    pypi = "ebrains_kg_core/ebrains_kg_core-0.9.10.tar.gz"

    maintainers = ["ioannistsanaktsidis", "olinux"]

    version('0.9.15', sha256='c672815ebcd6968f090620f68d85f0fbf282a83636c3a1845b078ef5ac5a06be')
    version('0.9.14', sha256='e898761abb999b09e5da49b25d13ffcadebaec7acd87ed079abe23f3bb12a5e7')
    version('0.9.13', sha256='25e155e9eb6c516dd33e29d9ff433c996630989a0f017c5920b66fe5334f2d82')
    version('0.9.12', sha256='244e52dffaec425bf65cf2d3d697969ec9036aaa028bf47fdddfb4911282f484')
    version("0.9.10", sha256="5b7022379f06468fd827a8eb7bddf86a182af188abfd11c84a47bbe21c6f8b7b")

    depends_on("py-setuptools", type="build")

    depends_on("py-requests", type=("build", "run"))
    depends_on("py-pydantic", type=("build", "run"))
    depends_on("py-jinja2", type=("build", "run"))
