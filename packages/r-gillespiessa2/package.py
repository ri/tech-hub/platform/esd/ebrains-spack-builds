# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class RGillespiessa2(RPackage):
    """A fast, scalable, and versatile framework for simulating large systems with Gillespie's Stochastic Simulation Algorithm ('SSA'). This package is the spiritual successor to the 'GillespieSSA' package originally written by Mario Pineda-Krch. Benefits of this package include major speed improvements (>100x), easier to understand documentation, and many unit tests that try to ensure the package works as intended. Cannoodt and Saelens et al. (2021) <doi:10.1038/s41467-021-24152-2>."""

    homepage = "https://cran.r-project.org/package=GillespieSSA2"
    cran = "GillespieSSA2"

    version("0.3.0", sha256="206497ae7be8ff88a5edafe81b0295eb4b14109c546878ace16a7396a0035f72")

    depends_on("r-assertthat")
    depends_on("r-dplyr")
    depends_on("r-dynutils")
    depends_on("r-matrix")
    depends_on("r-purrr")
    depends_on("r-rcpp")
    depends_on("r-rcppxptrutils")
    depends_on("r-readr")
    depends_on("r-rlang")
    depends_on("r-stringr")
    depends_on("r-tidyr")

