# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *

class BiobbCommon(PythonPackage):
    """Biobb_common is the base package required to use the biobb packages"""

    # Homepage and download url
    homepage = "https://github.com/bioexcel/biobb_common"
    git = 'https://github.com/bioexcel/biobb_common.git'
    url = 'https://github.com/bioexcel/biobb_common/archive/refs/tags/v4.0.0.tar.gz'

    # Set the gitlab accounts of this package maintainers
    maintainers = ['dbeltran']

    # Versions
    version('master', branch='master')
    version('4.0.0', sha256='fff990dce42ded2af3d587567dbf5321b1498f12f24d04d62003f9869d6eb8fe')

    # Dependencies
    depends_on('py-setuptools')
    depends_on('python@3.8:', type=('build', 'run'))
    depends_on('py-pyyaml', type=('build', 'run'))
    depends_on('py-requests', type=('build', 'run'))
    depends_on('py-biopython@1.78:1.80', type=('build', 'run'))

    # Custom patch to enable python 3.10.8 for this package
    def patch(self):
        filter_file("    python_requires='>=3.7,<=3.10',", "    python_requires='>=3.7,<3.11',", "setup.py")

    # Test
    @run_after('install')
    @on_package_attributes(run_tests=True)
    def check_install (self):
        python("-c", 'import biobb_common')
