# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *

class Nglview(PythonPackage):
    """An IPython/Jupyter widget to interactively view molecular structures and trajectories. 
    Utilizes the embeddable NGL Viewer for rendering."""

    # Homepage and download url
    homepage = "https://github.com/nglviewer/nglview"
    git = 'https://github.com/nglviewer/nglview.git'
    url = 'https://github.com/nglviewer/nglview/archive/refs/tags/v3.0.4.tar.gz'

    # Set the gitlab accounts of this package maintainers
    maintainers = ['dbeltran']

    # Versions
    version('master', branch='master')
    version('3.0.4', sha256='78b4413b796965a94045df0d584ec51e256c3dca5f366020439fe7e9744ce61b')

    # Dependencies
    depends_on('python@3.8:', type=('build', 'run'))
    depends_on('py-setuptools')
    depends_on('py-jupyter-packaging')
    depends_on('py-versioneer')
    depends_on('py-numpy', type=('run'))
    depends_on('py-ipywidgets', type=('run'))

    # Test
    @run_after('install')
    @on_package_attributes(run_tests=True)
    def check_install (self):
        python("-c", 'import nglview')
