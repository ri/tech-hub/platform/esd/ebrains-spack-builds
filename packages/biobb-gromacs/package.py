# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *

class BiobbGromacs(PythonPackage):
    """Biobb_gromacs is the Biobb module collection to perform molecular
    dynamics simulations using Gromacs"""

    # Homepage and download url
    homepage = "https://github.com/bioexcel/biobb_gromacs"
    git = 'https://github.com/bioexcel/biobb_gromacs.git'
    url = 'https://github.com/bioexcel/biobb_gromacs/archive/refs/tags/v4.0.0.tar.gz'

    # Set the gitlab accounts of this package maintainers
    maintainers = ['dbeltran']

    # Versions
    version('master', branch='master')
    version('4.0.0', sha256='87f9079ec8b72ff43cd1b388a06fee5b1f64bb4080ffab110941f3c6d155ef0b')

    # Dependencies
    depends_on('python@3.8:', type=('build', 'run'))
    depends_on('biobb-common')
    depends_on('gromacs')

    # Patching to enable python 3.10 (not official, might not be stable)
    def patch(self):
        filter_file("    python_requires='>=3.7,<=3.10',", "    python_requires='>=3.7,<3.11',", "setup.py")

    # Test
    @run_after('install')
    @on_package_attributes(run_tests=True)
    def check_install (self):
        python("-c", 'import biobb_gromacs')
