from spack.package import *


class PyPytestTornasync(PythonPackage):
    """
    py.test plugin for testing Python 3.5+ Tornado code
    """

    homepage = "https://github.com/eukaryote/pytest-tornasync"
    pypi = "pytest-tornasync/pytest-tornasync-0.6.0.post2.tar.gz"
    maintainers = ['ldomide', 'adrianciu']

    license('http://www.opensource.org/licenses/mit-license.php')

    version("0.6.0.post2", sha256="d781b6d951a2e7c08843141d3ff583610b4ea86bfa847714c76edefb576bbe5d")

    depends_on('python@3.5:', type=('build', 'run'))
    depends_on('py-setuptools', type=('build'))
    depends_on('py-pytest', type=('build', 'run'))
    depends_on('py-tornado@5.0:', type=('build', 'run'))

