# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


_JAR_URL_7_3_0 = "https://github.com/SpiNNakerManchester/JavaSpiNNaker/releases/download/7.3.0/spinnaker-exe.jar"
_JAR_SHA256_7_3_0 = "8fea399e835d053eb9b9b8b6f4752475d19cc3995389ca544f3ad1758007edbf"

_JAR_URL_7_0_0 = "https://github.com/SpiNNakerManchester/JavaSpiNNaker/releases/download/7.0.0/spinnaker-exe.jar"
_JAR_SHA256_7_0_0 = "2d909c7fb3aa15886acf26febb1bd48e25db0c347a231944aa6a5f86107bb55b"

class PySpinnfrontendcommon(PythonPackage):
    """This package provides utilities for specifying binary data
    algorithmically, and executing the specifications to produce the data."""

    homepage = "https://github.com/SpiNNakerManchester/SpiNNFrontEndCommon"
    pypi = "SpiNNFrontEndCommon/SpiNNFrontEndCommon-1!7.0.0.tar.gz"

    def url_for_version(self, version):
        name = "spinnfrontendcommon" if version >= Version("7.2.0") else "SpiNNFrontEndCommon"
        url = "https://pypi.org/packages/source/s/SpiNNFrontEndCommon/{}-1!{}.tar.gz"
        return url.format(name, version)

    version("7.3.0", sha256="c3aea0160525c4f08bc74244f219a9664a06aa70876cfb68944c7d6378daf161")
    version("7.0.0", sha256="07539734ed0105472d06d655bbd92e149ef44c77c388fcca28857558faa6dd10")

    depends_on("python@3.8:", type=("build", "run"), when="@7.3.0:")
    depends_on("python@3.7:", type=("build", "run"), when="@7.0.0:")

    depends_on("py-spinnman@7.3.0", type=("build", "run"), when="@7.3.0")
    depends_on("py-spinnaker-pacman@7.3.0", type=("build", "run"), when="@7.3.0")
    depends_on("py-spalloc@7.3.0", type=("build", "run"), when="@7.3.0")

    depends_on("py-spinnman@7.0.0", type=("build", "run"), when="@7.0.0")
    depends_on("py-spinnaker-pacman@7.0.0", type=("build", "run"), when="@7.0.0")
    depends_on("py-spalloc@7.0.0", type=("build", "run"), when="@7.0.0")

    depends_on("py-scipy@0.16.0:", type=("build", "run"))
    depends_on("py-ebrains-drive@0.5.1:", type=("build", "run"))

    depends_on("java@11:")

    resource(name="spinnaker-exe.jar", url=_JAR_URL_7_0_0, checksum=_JAR_SHA256_7_0_0, expand=False, placement="resource_root/JavaSpiNNaker/SpiNNaker-front-end/target", when="@7.0.0")
    resource(name="spinnaker-exe.jar", url=_JAR_URL_7_3_0, checksum=_JAR_SHA256_7_3_0, expand=False, placement="resource_root/JavaSpiNNaker/SpiNNaker-front-end/target", when="@7.3.0")

    def install(self, spec, prefix):
        super(PySpinnfrontendcommon, self).install(spec, prefix)

        # Work out the python version installed, so we know where to put
        # the java code!
        python_version = spec["python"].version.up_to(2)
        python_lib = prefix.lib.join(f"python{python_version}")
        install_tree("resource_root", python_lib)
