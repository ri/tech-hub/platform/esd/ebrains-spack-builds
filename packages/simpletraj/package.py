# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *

class Simpletraj(PythonPackage):
    """Lightweight coordinate-only trajectory reader based on code from GROMACS, MDAnalysis and VMD."""

    # Homepage and download url
    homepage = "https://github.com/arose/simpletraj"
    git = 'https://github.com/arose/simpletraj.git'
    url = 'https://github.com/arose/simpletraj/archive/refs/tags/v0.3.tar.gz'

    # Set the gitlab accounts of this package maintainers
    maintainers = ['dbeltran']

    # Versions
    version('master', branch='master')
    version('0.3', sha256='9ee9b5f3e387f8f8eb74b11f5c5d60bab6f601d190b40e38a7b31afddc3574d0')

    # Dependencies
    depends_on('python@3.8:', type=('build', 'run'))
    depends_on('py-setuptools')
    depends_on('py-numpy')

    # Test
    @run_after('install')
    @on_package_attributes(run_tests=True)
    def check_install (self):
        python("-c", 'import simpletraj')
