# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *

class BiobbIo(PythonPackage):
    """Biobb_io is the Biobb module collection to fetch data to be
    consumed by the rest of the Biobb building blocks"""

    # Homepage and download url
    homepage = "https://github.com/bioexcel/biobb_io"
    git = 'https://github.com/bioexcel/biobb_io.git'
    url = 'https://github.com/bioexcel/biobb_io/archive/refs/tags/v4.0.0.tar.gz'

    # Set the gitlab accounts of this package maintainers
    maintainers = ['dbeltran']

    # Versions
    version('master', branch='master')
    version('4.0.0', sha256='600a30f14b1a0e21f57775ba1be695e1595f5702237415fe90d7c531b5a0408a')

    # Dependencies
    depends_on('biobb-common')
    depends_on('python@3.8:', type=('build', 'run'))

    # Patching to enable python 3.10 (not official, might not be stable)
    def patch(self):
        filter_file("    python_requires='>=3.7,<3.10',", "    python_requires='>=3.7,<3.11',", "setup.py")
        filter_file(
            "'Programming Language :: Python :: 3.9'",
            "'Programming Language :: Python :: 3.9',\r\n        "
            "'Programming Language :: Python :: 3.10'",
            "setup.py",
        )

    # Test
    @run_after('install')
    @on_package_attributes(run_tests=True)
    def check_install (self):
        python("-c", 'import biobb_io')
