# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class RSbtabvfgen(RPackage):
    """This package converts SBtab files (with one table per tsv file) into three other formats: vfgen, sbml, Neuron's MOD. It also saves its findings in a free form, as text files. SBtab files typically contain a reaction network model (systems biology). The output is more useful for simulations."""

    homepage = "https://github.com/icpm-kth/SBtabVFGEN"
    url = "https://github.com/icpm-kth/SBtabVFGEN/archive/refs/tags/v0.1.tar.gz"

    maintainers = ["akramer", "oliviaeriksson"]

    version("0.1", sha256="499c51abee75ca5961a41608c9fb09ad523e12aae276f5f67bffa313244cff28")

    depends_on("r-pracma")
    depends_on("r-hdf5r")
    depends_on("sbml+r")
