# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PySpinnmachine(PythonPackage):
    """This package is used to provide a Python representation of a SpiNNaker
    machine."""

    homepage = "https://github.com/SpiNNakerManchester/SpiNNMachine"
    pypi = "SpiNNMachine/SpiNNMachine-1!7.0.0.tar.gz"

    def url_for_version(self, version):
        name = "spinnmachine" if version >= Version("7.2.0") else "SpiNNMachine"
        url = "https://pypi.org/packages/source/s/SpiNNMachine/{}-1!{}.tar.gz"
        return url.format(name, version)

    version("7.3.0", sha256="7c23def7deac54d56e23f4679c2317ddd053e6f6632c81ddf497fe021f37960c")
    version("7.0.0", sha256="5da374fd9208287799fbc324136fe5954dd1b370792ea81ea10d4537643272ad")

    depends_on("python@3.8:", type=("build", "run"), when="@7.3.0:")
    depends_on("python@3.7:", type=("build", "run"), when="@7.0.0:")

    depends_on("py-spinnutilities@7.3.0", type=("build", "run"), when="@7.3.0")
    depends_on("py-spinnutilities@7.0.0", type=("build", "run"), when="@7.0.0")
