from spack.package import *


class PyTvbExtXircuits(PythonPackage):
    """"
    This is a jupyterlab extension built as a prototype for building EBRAINS (including TVB simulator, Siibra API) workflows
    in a visual and interactive manner. It extends the already existent Xircuits jupyterlab extension by adding new components and new features on top.
    """

    homepage = "https://www.thevirtualbrain.org/"
    pypi = "tvb-ext-xircuits/tvb-ext-xircuits-1.1.0.tar.gz"
    maintainers = ['ldomide', 'adrianciu']

    version("1.1.0", sha256="37c71be6ac8e4abb91501b2eb788167f6142eefd2f36ef061d760d2e61828afd")

    depends_on('py-docutils', type=("build", "run"))
    depends_on('py-ipykernel', type=("build", "run"))
    depends_on('py-packaging', type=("build", "run"))
    depends_on('py-tornado@6.1.0:', type=("build", "run"))
    depends_on('py-jupyter-core', type=("build", "run"))
    depends_on('py-jupyter-packaging', type=("build", "run"))
    depends_on('py-jupyterlab-server@2.11.1:3', type=("build", "run"))
    depends_on('py-jupyter-server', type=("build", "run"))
    depends_on('py-notebook-shim@0.1:', type=("build", "run"))
    depends_on('py-jinja2@3.0.3:', type=("build", "run"))
    depends_on('py-jupyterlab@3.4.7:3', type=("build", "run"))
    depends_on('py-jupyterlab-widgets', type=("build", "run"))
    depends_on('py-nbformat', type=("build", "run"))
    depends_on('py-numpy', type=("build", "run"))
    depends_on('py-requests', type=("build", "run"))
    depends_on('py-gitpython', type=("build", "run"))
    depends_on('py-pygithub', type=("build", "run"))
    depends_on('py-pyunicore', type=("build", "run"))
    depends_on('py-siibra', type=("build", "run"))
    depends_on('py-tqdm', type=("build", "run"))
    depends_on('py-tvb-library', type=("build", "run"))
    depends_on('py-tvb-gdist', type=("build", "run"))
    depends_on('py-tvb-framework', type=("build", "run"))
    depends_on('py-tvb-ext-bucket', type=("build", "run"))
    depends_on('py-tvb-ext-unicore', type=("build", "run"))
    depends_on('py-tvb-widgets@1.0:', type=("build", "run"))

    depends_on('py-pytest', type='test')

    @run_after('install')
    @on_package_attributes(run_tests=True)
    def install_test(self):
        pytest = which('pytest')
        pytest('--ignore', 'tvbextxircuits/tests/xircuits/test_generate_description.py')
